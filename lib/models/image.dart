class ImageBg {
  String backgroundImageBg;
  String sport;

  ImageBg({this.backgroundImageBg, this.sport});

  factory ImageBg.fromJson(Map<String, dynamic> json) {
    return ImageBg(
      backgroundImageBg: json['strSportThumb'],
      sport: json['strSport'],
    );
  }
}
