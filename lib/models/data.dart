import 'package:flutter_sportDB/models/sport.dart';

class Data {
  List<Sport> countrys;

  Data({this.countrys});

  factory Data.fromJson(Map<String, dynamic> json) {
    Iterable list = json['countrys'];
    try {
      List<Sport> sports = list.map((e) => Sport.fromJson(e)).toList();
      return Data(
        countrys: sports,
      );
    } catch (e) {
      print(e);
      return Data(
        countrys: [],
      );
    }
  }
}
