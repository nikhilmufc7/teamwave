import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sportDB/models/data.dart';
import 'package:flutter_sportDB/models/imagedata.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';

class DetailScreen extends StatefulWidget {
  DetailScreen({Key key, this.countryName}) : super(key: key);

  final String countryName;

  @override
  _DetailScreenState createState() => _DetailScreenState();
}

class _DetailScreenState extends State<DetailScreen> {
  Future<Data> searchData;

  bool _isSearching = false;

  // Fetching data by country name

  Future<Data> fetchAllSports() async {
    final response = await http.get(
        'https://www.thesportsdb.com/api/v1/json/1/search_all_leagues.php?c=${widget.countryName}');

    if (response.statusCode == 200) {
      return Data.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to load album');
    }
  }

  // fetching background image
  Future<Map<String, dynamic>> fetchBackgroundImage() async {
    final response = await http
        .get('https://www.thesportsdb.com/api/v1/json/1/all_sports.php');

    if (response.statusCode == 200) {
      return ImageData.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to load data');
    }
  }

  // searching

  Future<Data> searchTerm(String query) async {
    if (query.isEmpty) {
      setState(() {
        _isSearching = false;
      });
      return null;
    }

    _isSearching = true;
    final response = await http.get(
        'https://www.thesportsdb.com/api/v1/json/1/search_all_leagues.php?c=${widget.countryName}&s=$query');

    if (response.statusCode == 200) {
      return Data.fromJson(json.decode(response.body));
    } else {
      throw Exception('failed to load');
    }
  }

  // mapping background image

  String getBackgroundImageForSport(String sportType, dynamic data) {
    return data[sportType]["strSportThumb"];
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.countryName),
        backgroundColor: Colors.deepPurpleAccent,
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              decoration: InputDecoration(
                  hintText: 'Search Leagues',
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(15))),
              onSubmitted: (text) {
                setState(() {
                  searchData = searchTerm(text);
                });
              },
            ),
          ),
          if (_isSearching == false)
            Expanded(
              child: FutureBuilder(
                future: Future.wait([
                  fetchAllSports(),
                  fetchBackgroundImage(),
                ]),
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  if (snapshot.hasData) {
                    dynamic sportsData = snapshot.data[0];
                    dynamic imageData = snapshot.data[1];
                    return ListView.builder(
                      itemCount: sportsData.countrys.length,
                      itemBuilder: (BuildContext context, int index) {
                        return _leagueBox(
                            sportsData.countrys[index].leagueName,
                            sportsData.countrys[index].leagueLogo,
                            sportsData.countrys[index].twitter,
                            sportsData.countrys[index].facebook,
                            getBackgroundImageForSport(
                                sportsData.countrys[index].sportName,
                                imageData));
                      },
                    );
                  } else if (snapshot.hasError) {
                    return Text("${snapshot.error}");
                  }
                  return Center(child: CircularProgressIndicator());
                },
              ),
            ),
          if (_isSearching == true)
            Expanded(
              child: FutureBuilder(
                future: Future.wait([
                  searchData,
                  fetchBackgroundImage(),
                ]),
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  if (snapshot.hasData) {
                    dynamic sportsData = snapshot.data[0];
                    dynamic imageData = snapshot.data[1];
                    return ListView.builder(
                      itemCount: sportsData.countrys.length,
                      itemBuilder: (BuildContext context, int index) {
                        return _leagueBox(
                            sportsData.countrys[index].leagueName,
                            sportsData.countrys[index].leagueLogo,
                            sportsData.countrys[index].twitter,
                            sportsData.countrys[index].facebook,
                            getBackgroundImageForSport(
                                sportsData.countrys[index].sportName,
                                imageData));
                      },
                    );
                  } else if (snapshot.hasError) {
                    return Text("${snapshot.error}");
                  }
                  return Center(
                      child: Center(child: Text('Seach result not found')));
                },
              ),
            ),
        ],
      ),
    );
  }

  Widget _leagueBox(String leagueName, String leagueLogo, String twitter,
      String facebook, String backgroundImage) {
    if (backgroundImage.length > 0) {
      print(backgroundImage);
    }
    return Container(
      height: 120,
      width: double.infinity,
      margin: EdgeInsets.all(10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        image: DecorationImage(
            fit: BoxFit.cover, image: NetworkImage(backgroundImage)),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          FittedBox(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                leagueName,
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 16),
              ),
            ),
          ),
          ListTile(
            title: Row(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                FlatButton(
                  padding: EdgeInsets.all(0),
                  onPressed: () async {
                    var url = 'https://' + twitter;
                    if (await canLaunch(url)) {
                      await launch(url);
                    } else {
                      throw 'Could not launch $url';
                    }
                  },
                  child: Container(
                    height: 30,
                    width: 30,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                          image: NetworkImage(
                              'https://3.bp.blogspot.com/-NxouMmz2bOY/T8_ac97cesI/AAAAAAAAGg0/e3vY1_bdnbE/s1600/Twitter+logo+2012.png')),
                    ),
                  ),
                ),
                FlatButton(
                  onPressed: () async {
                    var url = 'https://' + facebook;
                    if (await canLaunch(url)) {
                      await launch(url);
                    } else {
                      throw 'Could not launch $url';
                    }
                  },
                  child: Container(
                    height: 30,
                    width: 30,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                          image: NetworkImage(
                              'https://facebookbrand.com/wp-content/uploads/2019/04/f_logo_RGB-Hex-Blue_512.png?w=512&h=512')),
                    ),
                  ),
                ),
              ],
            ),
            trailing: Container(
              width: 70,
              height: 30,
              decoration: BoxDecoration(
                  image:
                      DecorationImage(image: NetworkImage(leagueLogo ?? ''))),
            ),
          )
        ],
      ),
    );
  }
}
