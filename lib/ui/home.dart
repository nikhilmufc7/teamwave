import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sportDB/ui/detail_screen.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('The Sports DB'),
        backgroundColor: Colors.deepPurpleAccent,
      ),
      body: Column(
        children: [
          SizedBox(
            height: 25,
          ),
          FittedBox(
            fit: BoxFit.cover,
            child: Row(
              children: <Widget>[
                _countries(context, 'India', () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              DetailScreen(countryName: 'india')));
                }, 'https://www.thestatesman.com/wp-content/uploads/2017/08/1486448836-india-flag517.jpg'),
                _countries(context, 'England', () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              DetailScreen(countryName: 'england')));
                }, 'https://www.theflagshop.co.uk/media/catalog/product/cache/1/thumbnail/9df78eab33525d08d6e5fb8d27136e95/e/n/england-flag-std.jpg'),
              ],
            ),
          ),
          FittedBox(
            fit: BoxFit.cover,
            child: Row(
              children: <Widget>[
                _countries(context, 'Argentina', () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              DetailScreen(countryName: 'argentina')));
                }, 'https://blogpatagonia.australis.com/wp-content/uploads/2019/10/argentina-flag-and-symbols.jpg'),
                _countries(context, 'China', () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              DetailScreen(countryName: 'china')));
                }, 'https://www.ledgerinsights.com/wp-content/uploads/2019/04/china-flag-810x476.jpg'),
              ],
            ),
          ),
          FittedBox(
            fit: BoxFit.cover,
            child: Row(
              children: <Widget>[
                _countries(context, 'Australia', () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              DetailScreen(countryName: 'australia')));
                }, 'https://cdn.edarabia.com/wp-content/uploads/2019/10/australia-flag.jpg'),
                _countries(context, 'Canada', () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              DetailScreen(countryName: 'canada')));
                }, 'https://www.theflagshop.co.uk/media/catalog/product/cache/1/thumbnail/9df78eab33525d08d6e5fb8d27136e95/c/a/canada-flag-std_1.jpg'),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _countries(
      context, String _country, Function _onPressed, String imageUrl) {
    Size size = MediaQuery.of(context).size;
    double screenHeight = size.height;
    double screenWidth = size.width;

    return CupertinoButton(
      child: Container(
        height: screenHeight / 6.0,
        width: screenWidth / 2.3,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage(imageUrl),
            fit: BoxFit.cover,
          ),
          borderRadius: BorderRadius.circular(12),
        ),
        child: Container(
          margin: EdgeInsets.fromLTRB(15, 5, 0, 0),
          child: Text(
            _country,
            style: TextStyle(
                color: Colors.white,
                fontSize: 20,
                fontFamily: 'Montserrat',
                fontWeight: FontWeight.bold),
          ),
        ),
      ),
      onPressed: _onPressed,
    );
  }
}
