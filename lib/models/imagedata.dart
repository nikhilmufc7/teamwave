class ImageData {
  static fromJson(Map<String, dynamic> json) {
    Iterable list = json['sports'];
    try {
      Map<String, dynamic> nameOfSports = Map();
      for (var sport in list) {
        Map imageBg = Map();
        imageBg["strSport"] = sport["strSport"];
        imageBg["strSportThumb"] = sport["strSportThumb"];
        nameOfSports[sport["strSport"]] = imageBg;
      }
      return nameOfSports;
    } catch (e) {
      print(e);
      return {};
    }
  }
}
