class Sport {
  String leagueName;
  String leagueLogo;
  String twitter;
  String facebook;
  String sportName;

  Sport(
      {this.leagueName,
      this.leagueLogo,
      this.twitter,
      this.facebook,
      this.sportName});

  factory Sport.fromJson(Map<String, dynamic> json) {
    return Sport(
      leagueName: json['strLeague'],
      leagueLogo: json['strLogo'],
      twitter: json['strTwitter'],
      facebook: json['strFacebook'],
      sportName: json['strSport'],
    );
  }
}
